module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'react-app',
    'airbnb',
  ],
  parserOptions: {
    ecmaVersion: 13,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  settings: {
    'import/resolver': {
      node: {
        paths: [
          '.',
        ],
        extensions: [
          '.js',
          '.jsx',
        ],
      },
    },
  },
  rules: {
    'comma-dangle': 'off',
    'jsx-a11y/anchor-is-valid': 'off',
    'max-len': ['error', {
      code: 120,
      comments: 120,
      ignoreTemplateLiterals: true,
    }],
    'no-console': 'off',
    'no-plusplus': 'off',
    'no-restricted-syntax': [
      'warn', 'FunctionExpression', 'LabeledStatement', 'WithStatement'
    ],
    'react/jsx-filename-extension': 'off',
    'react/jsx-props-no-spreading': 'off',
    'react/react-in-jsx-scope': 'off',
    'react/require-default-props': 'off',
    'no-param-reassign': 'off',
    'import/extensions': [
      'error', 'never', { ignorePackages: true }
    ],
    'import/prefer-default-export': 'off',
    'react/jsx-max-props-per-line': [1, { maximum: 1, when: 'always' }],
    'no-shadow': 'off',
    'react/function-component-definition': [2, { namedComponents: 'arrow-function' }]
  },
};
